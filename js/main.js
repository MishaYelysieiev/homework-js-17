$(document).ready(function () {
    $("#add").click(() => {
        $("#add").hide();
        $("body").append("<input type='text' placeholder='Enter the diameter'>");
        $("body").append("<button class='button' id='create'>Create</button>");
        $("#create").click(() => {
            let diam = +$("input:first").val();
            $("#create,input").hide();
            for (let i = 0; i < 100; i++) {
                let letters = '0123456789ABCDEF';
                let color = "#";
                for (let k = 0; k < 6; k++) {
                    color += letters[Math.floor(Math.random() * 16)];
                };
                $("body").append(`<div class="circle" style=width:${diam}px;height:${diam}px;border-radius:50%;display:inline-block;background-color:${color};margin:10px></div>`);
            };
            $("body").click((e) => {
                if ($(e.target).hasClass("circle")) {
                    $(e.target).css("opacity", "0");
                };
            });
        });
    });
});